// https://plopjs.com/documentation/#installation
// Don't change this file name. should be `plopfile.js`
import { NodePlopAPI } from 'plop';

const componentTemplates = [
  {
    // Add a new file
    type: "add",
    // Path for the new file
    path: "{{path}}/{{pascalCase name}}/{{pascalCase name}}.tsx",
    // Handlebars template used to generate content of new file
    templateFile: "_templates/Component/Component.tsx.hbs",
  },
  {
    type: "add",
    path: "{{path}}/{{pascalCase name}}/index.ts",
    templateFile: "_templates/Component/index.ts.hbs",
  },
  {
    type: "add",
    path: "{{path}}/{{pascalCase name}}/{{pascalCase name}}.const.ts",
    templateFile: "_templates/Component/Component.const.ts.hbs",
  },
];

const styleTemplate = {
  type: "add",
  path: "{{path}}/{{pascalCase name}}/{{pascalCase name}}.style.tsx",
  templateFile: "_templates/Component/Component.style.tsx.hbs",
};

const testTemplate = {
  type: "add",
  path: "{{path}}/{{pascalCase name}}/{{pascalCase name}}.test.ts",
  templateFile: "_templates/Component/Component.test.ts.hbs",
};

const componentGenerator = {
  description: "Create a component",
  // User input prompts provided as arguments to the template
  prompts: [
    {
      // Raw text input
      type: "input",
      // Variable name for this input
      name: "name",
      // Prompt to display on command line
      message: "Component name",
    },
    {
      type: "confirm",
      name: "withStyle",
      message: "Add style file?",
      default: true,
    },
    {
      type: "confirm",
      name: "withTestFile",
      message: "Add test file?",
      default: true,
    },
    {
      type: "confirm",
      name: "withMobx",
      message: "Connect component to store?",
      default: false,
    },
    {
      type: "input",
      name: "storeName",
      message: "store name",
      when: (promptResult) => promptResult.withMobx,
    },
    {
      type: "input",
      name: "path",
      message: "Path [no need to define the component name])",
      default: "src/components",
    },
  ],
  actions: (promptResult) => {
    if (promptResult.withStyle) {
      componentTemplates.push(styleTemplate);
    }
    if (promptResult.withTestFile) {
      componentTemplates.push(testTemplate);
    }

    return componentTemplates;
  },
};

module.exports = (plop: NodePlopAPI) => {
  plop.setGenerator("component", componentGenerator);
};
